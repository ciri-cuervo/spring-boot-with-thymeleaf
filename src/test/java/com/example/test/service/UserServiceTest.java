/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.test.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.User;
import com.example.domain.UserRepository;
import com.example.domain.types.AuthorityType;
import com.example.domain.types.UserStatusType;
import com.example.service.PathManagerService;
import com.example.service.UserService;
import com.example.util.ApplicationUtils;

/**
 * {@link UserService} unit test.
 * 
 * @author ciri-cuervo
 * 
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class UserServiceTest {

	private static final String USERNAME_1 = "user_1";
	private static final String PASSWORD_1 = "123qwe";
	private static final String EMAIL_1 = "email_1@email.com";

	private static final String USERNAME_2 = "user_2";
	private static final String PASSWORD_2 = "456rty";
	private static final String EMAIL_2 = "email_2@email.com";

	@Spy
	@Autowired
	private Environment environment;

	@Mock
	private UserRepository userRepository;
	@Mock
	private PathManagerService pathManagerService;

	@InjectMocks
	private UserService userService = new UserService();

	private User user1;
	private User user2;
	private User actualUser;

	private int maxLoginAttempts;

	@PostConstruct
	public void postConstruct() {
		maxLoginAttempts =
				environment.getProperty("application.max-login-attempts", Integer.class, ApplicationUtils.DEFAULT_MAX_LOGIN_ATTEMPTS);
	}

	@Before
	public void setUp() throws Exception {

		// init @Mock attributes and inject @Mock and @Spy attributes to @InjectMocks instances
		MockitoAnnotations.initMocks(this);

		// as it is not autowired we force the post construction method
		userService.postConstruct();

		user1 = new User();
		user1.setId(1L);
		user1.setUsername(USERNAME_1);
		user1.setPassword(PASSWORD_1);
		user1.setEmail(EMAIL_1);
		user1.setStatus(UserStatusType.ACTIVE);
		user1.addAuthority(AuthorityType.ROLE_USER);

		user2 = new User();
		user2.setId(2L);
		user2.setUsername(USERNAME_2);
		user2.setPassword(PASSWORD_2);
		user2.setEmail(EMAIL_2);
		user2.setStatus(UserStatusType.ACTIVE);
		user2.addAuthority(AuthorityType.ROLE_USER);

		actualUser = null;

		doAnswer(new Answer<User>() {
			@Override
			public User answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				actualUser = (User) args[0];
				return actualUser;
			}
		}).when(userRepository).save(any(User.class));

		doReturn(Optional.of(user1)).when(userRepository).findByEmail(EMAIL_1);
		doReturn(Optional.of(user1)).when(userRepository).findByUsername(USERNAME_1);
		doReturn(Optional.of(user2)).when(userRepository).findByEmail(EMAIL_2);
		doReturn(Optional.of(user2)).when(userRepository).findByUsername(USERNAME_2);
		doNothing().when(userRepository).delete(user1);
		doNothing().when(userRepository).delete(user2);
		doReturn(Paths.get("~/")).when(pathManagerService).deleteUserStoragePath(user1);
		doReturn(Paths.get("~/")).when(pathManagerService).deleteUserStoragePath(user2);
	}

	@Test
	public void testFindByUsernameOrEmail() {
		Optional<User> actualUser1Username = userService.findByUsernameOrEmail(USERNAME_1);
		Optional<User> actualUser1Email = userService.findByUsernameOrEmail(EMAIL_1);
		Optional<User> actualUser2Username = userService.findByUsernameOrEmail(USERNAME_2);
		Optional<User> actualUser2Email = userService.findByUsernameOrEmail(EMAIL_2);

		assertTrue(actualUser1Username.isPresent());
		assertTrue(actualUser1Email.isPresent());
		assertTrue(actualUser2Username.isPresent());
		assertTrue(actualUser2Email.isPresent());

		assertEquals(user1.getUsername(), actualUser1Username.get().getUsername());
		assertEquals(user1.getEmail(), actualUser1Username.get().getEmail());
		assertEquals(user1.getUsername(), actualUser1Email.get().getUsername());
		assertEquals(user1.getEmail(), actualUser1Email.get().getEmail());

		assertEquals(user2.getUsername(), actualUser2Username.get().getUsername());
		assertEquals(user2.getEmail(), actualUser2Username.get().getEmail());
		assertEquals(user2.getUsername(), actualUser2Email.get().getUsername());
		assertEquals(user2.getEmail(), actualUser2Email.get().getEmail());
	}

	@Test
	public void testDeleteUser() throws IOException {

		userService.deleteUser(user1);

		verify(userRepository, times(1)).delete(user1);
		verify(pathManagerService, times(1)).deleteUserStoragePath(user1);
		verifyNoMoreInteractions(userRepository);
		verifyNoMoreInteractions(pathManagerService);
	}

	@Test
	public void testAddLoginAttempt() throws IOException {

		int remainingAttempts = userService.addLoginAttempt(USERNAME_1);

		assertNotNull(actualUser);
		assertEquals(maxLoginAttempts - 1, remainingAttempts);
		assertEquals(UserStatusType.ACTIVE, actualUser.getStatus());
		assertEquals(Integer.valueOf(1), actualUser.getLoginAttempts());

		for (int i = 1; i < maxLoginAttempts; i++) {
			remainingAttempts = userService.addLoginAttempt(USERNAME_1);
		}

		assertEquals(0, remainingAttempts);
		assertEquals(UserStatusType.LOCKED, actualUser.getStatus());
		assertEquals(Integer.valueOf(maxLoginAttempts), actualUser.getLoginAttempts());

		remainingAttempts = userService.addLoginAttempt(USERNAME_1);

		assertEquals(0, remainingAttempts);
		assertEquals(UserStatusType.LOCKED, actualUser.getStatus());
		assertEquals(Integer.valueOf(maxLoginAttempts + 1), actualUser.getLoginAttempts());
	}

}
