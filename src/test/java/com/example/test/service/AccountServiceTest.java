/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.test.service;

import static org.junit.Assert.*;
import static org.mockito.Matchers.*;
import static org.mockito.Mockito.*;

import java.time.LocalDateTime;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.core.env.Environment;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.EmailHash;
import com.example.domain.EmailHashRepository;
import com.example.domain.User;
import com.example.domain.UserRepository;
import com.example.domain.types.AuthorityType;
import com.example.domain.types.EmailHashType;
import com.example.domain.types.UserStatusType;
import com.example.service.AccountService;
import com.example.service.EmailHashService;
import com.example.util.ApplicationUtils;
import com.example.web.command.HashCommand;
import com.example.web.command.PasswordResetConfirmCommand;
import com.example.web.command.SignupCommand;

/**
 * {@link AccountService} unit test.
 * 
 * @author ciri-cuervo
 * 
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@Transactional
public class AccountServiceTest {

	private static final String USERNAME = "user";
	private static final String PASSWORD = "123qwe";
	private static final String EMAIL = "email@email.com";
	private static final String HASH = ApplicationUtils.createHash();

	@Autowired
	private Environment environment;

	@MockBean
	private UserRepository userRepository;
	@MockBean
	private EmailHashRepository emailHashRepository;
	@MockBean
	private EmailHashService emailHashService;

	@Autowired
	private AccountService accountService;

	private User user;
	private User actualUser;
	private EmailHash emailHash;

	private int hashExpireDuration;
	private int maxLoginAttempts;

	@PostConstruct
	public void postConstruct() {
		hashExpireDuration =
				environment.getProperty("application.hash-expire-duration", Integer.class, ApplicationUtils.DEFAULT_HASH_EXPIRE_HS);
		maxLoginAttempts =
				environment.getProperty("application.max-login-attempts", Integer.class, ApplicationUtils.DEFAULT_MAX_LOGIN_ATTEMPTS);
	}

	@Before
	public void setUp() throws Exception {

		user = new User();
		user.setUsername(USERNAME);
		user.setPassword(PASSWORD);
		user.setEmail(EMAIL);
		user.setStatus(UserStatusType.ACTIVE);
		user.addAuthority(AuthorityType.ROLE_USER);

		emailHash = new EmailHash();
		emailHash.setEmail(EMAIL);
		emailHash.setHash(HASH);
		emailHash.setType(EmailHashType.ACCOUNT_ACT);
		emailHash.setActive(true);

		actualUser = null;

		when(userRepository.save(any(User.class))).then(new Answer<User>() {
			@Override
			public User answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				actualUser = (User) args[0];
				return actualUser;
			}
		});

		when(userRepository.findByEmail(EMAIL)).thenReturn(Optional.of(user));
		when(emailHashRepository.findByHash(HASH)).thenReturn(Optional.of(emailHash));

		when(emailHashService.createEmailHash(anyString(), any())).then(new Answer<EmailHash>() {
			@Override
			public EmailHash answer(InvocationOnMock invocation) throws Throwable {
				Object[] args = invocation.getArguments();
				EmailHash emailHash = new EmailHash();
				emailHash.setEmail((String) args[0]);
				emailHash.setHash(ApplicationUtils.createHash());
				emailHash.setType((EmailHashType) args[1]);
				emailHash.setActive(true);
				emailHash.setExpire(LocalDateTime.now().plusHours(hashExpireDuration));
				return emailHash;
			}
		});

		when(emailHashRepository.deactivateByEmail(any())).then(new Answer<Integer>() {
			@Override
			public Integer answer(InvocationOnMock invocation) throws Throwable {
				emailHash.setActive(false);
				return -1;
			}
		});
	}

	@Test
	public void testCreateUser() {
		SignupCommand command = new SignupCommand();
		command.setUsername(USERNAME);
		command.setPassword(PASSWORD);
		command.setEmail(EMAIL);

		EmailHash actualEmailHash = accountService.createUser(command);

		assertNotNull(actualUser);
		assertNotNull(actualEmailHash);

		assertEquals(USERNAME, actualUser.getUsername());
		assertTrue(new BCryptPasswordEncoder().matches(PASSWORD, actualUser.getPassword()));
		assertEquals(EMAIL, actualUser.getEmail());
		assertEquals(UserStatusType.WAITING_ACTIVATION, actualUser.getStatus());
		assertEquals(1, actualUser.getAuthorities().size());
		assertTrue(actualUser.hasAuthority(AuthorityType.ROLE_USER));

		assertEquals(EMAIL, actualEmailHash.getEmail());
		assertNotNull(actualEmailHash.getHash());
		assertEquals(EmailHashType.ACCOUNT_ACT, actualEmailHash.getType());
		assertTrue(actualEmailHash.getActive());
		assertNotNull(actualEmailHash.getExpire());
		assertTrue(actualEmailHash.getExpire().isAfter(LocalDateTime.now()));
		assertTrue(actualEmailHash.getExpire().isBefore(LocalDateTime.now().plusHours(hashExpireDuration)));
	}

	@Test
	public void testActivateAccount() {
		user.setStatus(UserStatusType.WAITING_ACTIVATION);

		HashCommand command = new HashCommand();
		command.setHash(HASH);

		accountService.activateAccount(command);

		assertNotNull(actualUser);
		assertEquals(UserStatusType.ACTIVE, actualUser.getStatus());
		assertFalse(emailHash.getActive());
	}

	@Test
	public void testUnlockAccount() {
		user.setStatus(UserStatusType.LOCKED);
		user.setLoginAttempts(maxLoginAttempts + 1);

		HashCommand command = new HashCommand();
		command.setHash(HASH);

		accountService.unlockAccount(command);

		assertNotNull(actualUser);
		assertEquals(UserStatusType.ACTIVE, actualUser.getStatus());
		assertEquals(Integer.valueOf(0), actualUser.getLoginAttempts());
		assertFalse(emailHash.getActive());
	}

	@Test
	public void testChangePassword() {
		PasswordResetConfirmCommand command = new PasswordResetConfirmCommand();
		command.setHash(HASH);
		command.setPassword("NEW PASS");
		command.setRepeatPassword("NEW PASS");

		accountService.changePassword(command);

		assertNotNull(actualUser);
		assertTrue(new BCryptPasswordEncoder().matches("NEW PASS", actualUser.getPassword()));
		assertFalse(emailHash.getActive());
	}

}
