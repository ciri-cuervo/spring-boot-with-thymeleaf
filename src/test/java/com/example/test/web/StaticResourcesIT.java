/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.test.web;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * Static resources integration test.
 * 
 * @author ciri-cuervo
 * 
 */
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
public class StaticResourcesIT {

	private TestRestTemplate template = new TestRestTemplate();

	@Value("${local.server.port}")
	private int port;

	@Value("${info.versions.bootstrap}")
	private String bootstrapVersion;

	private String getBaseUrl() {
		return "http://localhost:" + port;
	}

	@Test
	public void testCss() throws Exception {
		ResponseEntity<String> response = template.getForEntity(getBaseUrl() + "/css/style.css", String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertThat("Wrong content", response.getBody(), containsString("body"));
	}

	@Test
	public void testBootstrapCss() throws Exception {
		String cssPath = "/webjars/bootstrap/" + bootstrapVersion + "/css/bootstrap.min.css";
		ResponseEntity<String> response = template.getForEntity(getBaseUrl() + cssPath, String.class);
		assertEquals(HttpStatus.OK, response.getStatusCode());
		assertThat("Wrong content", response.getBody(), containsString("body"));
	}

}
