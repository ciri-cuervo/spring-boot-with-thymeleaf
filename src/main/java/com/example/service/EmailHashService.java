/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.service;

import java.time.LocalDateTime;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.EmailHash;
import com.example.domain.EmailHashRepository;
import com.example.domain.types.EmailHashType;
import com.example.util.ApplicationUtils;

/**
 * Provides functionality to manage {@link EmailHash}.
 * 
 * @author ciri-cuervo
 * 
 */
@Service
@Transactional
public class EmailHashService {

	@Autowired
	private Environment environment;
	@Autowired
	private EmailHashRepository emailHashRepository;

	private int hashExpireDuration;

	@PostConstruct
	public void postConstruct() {
		hashExpireDuration =
				environment.getProperty("application.hash-expire-duration", Integer.class, ApplicationUtils.DEFAULT_HASH_EXPIRE_HS);
	}

	/**
	 * Create email hash.
	 */
	public EmailHash createEmailHash(String email, EmailHashType emailType) {
		EmailHash emailHash = new EmailHash();
		emailHash.setEmail(email);
		emailHash.setHash(ApplicationUtils.createHash());
		emailHash.setType(emailType);
		emailHash.setActive(true);
		emailHash.setExpire(LocalDateTime.now().plusHours(hashExpireDuration));
		emailHashRepository.deactivateByEmail(emailHash.getEmail());
		return emailHashRepository.save(emailHash);
	}

}
