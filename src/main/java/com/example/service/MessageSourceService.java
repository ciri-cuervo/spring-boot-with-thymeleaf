/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

/**
 * Wraps Spring {@link MessageSource} to automatically set the current context locale.
 * 
 * @author ciri-cuervo
 * 
 */
@Service
public class MessageSourceService {

	@Autowired
	private MessageSource messageSource;

	public String getMessage(String code, Object... args) {
		return messageSource.getMessage(code, args, LocaleContextHolder.getLocale());
	}

}
