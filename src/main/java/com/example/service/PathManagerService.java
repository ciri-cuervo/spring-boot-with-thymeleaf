/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.util.WebUtils;

import com.example.domain.User;
import com.example.util.DeletePathVisitor;

import lombok.extern.slf4j.Slf4j;

/**
 * Provides functionalities to manage system directories.
 * 
 * @author ciri-cuervo
 * 
 */
@Service
@Slf4j
public class PathManagerService {

	private static final String DEFAULT_TEMPRORAL_DIR = "spring-boot-app";
	private static final String DEFAULT_STORAGE_DIR = "files";
	private static final String USERS_DIR = "users";

	@Autowired
	private ServletContext servletContext;
	@Autowired
	private Environment environment;

	private Path defaultTemporalPath;
	private Path defaultStoragePath;
	private DeletePathVisitor deletePathVisitor = new DeletePathVisitor();

	@PostConstruct
	private void postConstruct() {
		defaultTemporalPath = WebUtils.getTempDir(servletContext).toPath().resolve(DEFAULT_TEMPRORAL_DIR);
		defaultStoragePath = defaultTemporalPath.resolve(DEFAULT_STORAGE_DIR);
	}

	/**
	 * Returns the path of the temporal directory.<br>
	 * Location can be set in {@code application.temporal-dir} property.
	 */
	public Path getTemporalPath() throws IOException {
		return getPath(environment.getProperty("application.tmporal-dir"), defaultTemporalPath);
	}

	/**
	 * Returns the path of the storage directory.<br>
	 * Location can be set in {@code application.storage-dir} property.
	 */
	public Path getStoragePath() throws IOException {
		return getPath(environment.getProperty("application.storage-dir"), defaultStoragePath);
	}

	/**
	 * Returns the users storage directory.
	 */
	public Path getUsersStoragePath() throws IOException {
		return Files.createDirectories(getStoragePath().resolve(USERS_DIR));
	}

	/**
	 * Returns the storage directory of a user.
	 */
	public Path getUserStoragePath(User user) throws IOException {
		return Files.createDirectories(getUsersStoragePath().resolve(user.getId().toString()));
	}

	/**
	 * Deletes the storage directory of a user.
	 */
	public Path deleteUserStoragePath(User user) throws IOException {
		Path path = getUsersStoragePath().resolve(user.getId().toString());
		log.info("Delete user storage directory [id={}, username={}, path={}]", user.getId(), user.getUsername(), path);
		return Files.walkFileTree(path, deletePathVisitor);
	}

	private static Path getPath(String directory, Path defaultPath) throws IOException {
		if (StringUtils.hasText(directory)) {
			Path path = Paths.get(directory);
			if (Files.isWritable(path)) {
				return path;
			}
		}
		return defaultPath;
	}

}
