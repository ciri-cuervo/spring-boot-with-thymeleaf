/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.service.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.User;
import com.example.service.UserService;

/**
 * Allows users to login by {@code username} or {@code email}.
 * 
 * @author ciri-cuervo
 * 
 */
@Service
@Transactional(readOnly = true)
public class UserDetailsServiceImpl implements UserDetailsService {

	@Autowired
	private UserService userService;

	@Override
	public User loadUserByUsername(final String username) throws UsernameNotFoundException {
		Optional<User> user = userService.findByUsernameOrEmail(username);
		return user.orElseThrow(() -> new UsernameNotFoundException(username + " not found"));
	}

}
