/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.service.security;

import java.io.IOException;

import javax.annotation.PostConstruct;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.stereotype.Service;

import com.example.service.UserService;
import com.example.util.ApplicationUtils;

/**
 * When user authentication fails:<br>
 * 
 * <ul>
 * <li>Add login attempt. Lock user account if reaches the max allowed.</li>
 * <li>Redirect to login page with error information.</li>
 * </ul>
 * 
 * @author ciri-cuervo
 * 
 */
@Service
public class FailureHandlerService extends SimpleUrlAuthenticationFailureHandler {

	private static final String DEFAULT_FAILURE_URL = "/account/login?error";

	@Autowired
	private Environment environment;
	@Autowired
	private UserService userService;

	private int loginAttemptsThreshold;

	@PostConstruct
	public void postConstruct() {
		loginAttemptsThreshold =
				environment.getProperty(
						"application.login-attempts-threshold", Integer.class, ApplicationUtils.DEFAULT_LOGIN_ATTEMPTS_THRESHOLD);
	}

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {

		String username = request.getParameter("username");
		int remainingAttempts = userService.addLoginAttempt(username);

		String failureUrl;
		if (remainingAttempts > -1 && remainingAttempts <= loginAttemptsThreshold) {
			failureUrl = DEFAULT_FAILURE_URL + "&r=" + remainingAttempts;
		} else {
			failureUrl = DEFAULT_FAILURE_URL;
		}

		// TODO audit this event

		saveException(request, exception);
		getRedirectStrategy().sendRedirect(request, response, failureUrl);
	}

}
