/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.service.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.stereotype.Service;

import com.example.domain.EmailHashRepository;
import com.example.domain.User;
import com.example.domain.UserRepository;

/**
 * When user authentication succeeds:<br>
 * 
 * <ul>
 * <li>CLear login attempts.</li>
 * <li>Deactivate all {@link com.example.domain.EmailHash EmailHash} for user.</li>
 * <li>Redirect to saved request.</li>
 * </ul>
 * 
 * @author ciri-cuervo
 * 
 */
@Service
public class SuccessHandlerService extends SavedRequestAwareAuthenticationSuccessHandler {

	private static final String DEFAULT_SUCCESS_URL = "/home";

	@Autowired
	private EmailHashRepository emailHashRepository;
	@Autowired
	private UserRepository userRepository;

	public SuccessHandlerService() {
		setDefaultTargetUrl(DEFAULT_SUCCESS_URL);
	}

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication)
			throws ServletException, IOException {

		User user = (User) authentication.getPrincipal();
		user.setLoginAttempts(0);
		user = userRepository.save(user);

		emailHashRepository.deactivateByEmail(user.getEmail());

		super.onAuthenticationSuccess(request, response, authentication);
	}

}
