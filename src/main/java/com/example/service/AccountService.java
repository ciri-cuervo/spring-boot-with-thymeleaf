/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.EmailHash;
import com.example.domain.EmailHashRepository;
import com.example.domain.User;
import com.example.domain.UserRepository;
import com.example.domain.types.AuthorityType;
import com.example.domain.types.EmailHashType;
import com.example.domain.types.UserStatusType;
import com.example.web.command.HashCommand;
import com.example.web.command.PasswordResetConfirmCommand;
import com.example.web.command.SignupCommand;

/**
 * Provides functionality to manage user accounts.
 * 
 * @author ciri-cuervo
 * 
 */
@Service
@Transactional
public class AccountService {

	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EmailHashRepository emailHashRepository;
	@Autowired
	private EmailHashService emailHashService;

	/**
	 * Create new user account.
	 */
	public EmailHash createUser(SignupCommand signupCommand) {
		User user = new User();
		user.setUsername(signupCommand.getUsername());
		user.setEmail(signupCommand.getEmail());
		user.setPassword(new BCryptPasswordEncoder().encode(signupCommand.getPassword()));
		user.addAuthority(AuthorityType.ROLE_USER);
		user.setStatus(UserStatusType.WAITING_ACTIVATION);
		user = userRepository.save(user);
		return emailHashService.createEmailHash(signupCommand.getEmail(), EmailHashType.ACCOUNT_ACT);
	}

	/**
	 * Activate user account.
	 */
	public void activateAccount(HashCommand command) {
		EmailHash emailHash = emailHashRepository.findByHash(command.getHash()).get();
		User user = userRepository.findByEmail(emailHash.getEmail()).get();
		user.setStatus(UserStatusType.ACTIVE);
		user.setLoginAttempts(0);
		user = userRepository.save(user);
		emailHashRepository.deactivateByEmail(emailHash.getEmail());
	}

	/**
	 * Unlock user account.
	 */
	public void unlockAccount(HashCommand command) {
		activateAccount(command);
	}

	/**
	 * Change user password.
	 */
	public void changePassword(PasswordResetConfirmCommand command) {
		EmailHash emailHash = emailHashRepository.findByHash(command.getHash()).get();
		User user = userRepository.findByEmail(emailHash.getEmail()).get();
		user.setPassword(new BCryptPasswordEncoder().encode(command.getPassword()));
		user = userRepository.save(user);
		emailHashRepository.deactivateByEmail(emailHash.getEmail());
	}

}
