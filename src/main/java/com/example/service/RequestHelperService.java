/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.service;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

import com.example.util.ApplicationUtils;

/**
 * Provides access to the current HTTP Request to services that don't have a reference to it.
 * 
 * @author ciri-cuervo
 * 
 */
@Service
@Scope(value = "request", proxyMode = ScopedProxyMode.TARGET_CLASS)
public class RequestHelperService {

	@Autowired
	private HttpServletRequest request;

	public HttpServletRequest getRequest() {
		return request;
	}

	/**
	 * Returns the base URL of the application: {@code http[s]://domain.com[:8080][/contextpath]}
	 */
	public String getBaseUrl() {
		return ApplicationUtils.getBaseUrl(request);
	}

	/**
	 * Obtain the request URI without the context path and with the query string appended.
	 */
	public String getRequestPathWithQuery() {
		return ApplicationUtils.getRequestPathWithQuery(request);
	}

}
