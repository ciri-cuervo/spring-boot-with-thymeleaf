/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.service;

import java.io.IOException;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.User;
import com.example.domain.UserRepository;
import com.example.domain.types.UserStatusType;
import com.example.util.ApplicationUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Provides functionality to manage {@link User}.
 * 
 * @author ciri-cuervo
 * 
 */
@Service
@Transactional
@Slf4j
public class UserService {

	@Autowired
	private Environment environment;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private PathManagerService pathManagerService;

	private int maxLoginAttempts;

	@PostConstruct
	public void postConstruct() {
		maxLoginAttempts =
				environment.getProperty("application.max-login-attempts", Integer.class, ApplicationUtils.DEFAULT_MAX_LOGIN_ATTEMPTS);
	}

	/**
	 * Gets a {@link User} by {@code username} or {@code email}.
	 */
	@Transactional(readOnly = true)
	public Optional<User> findByUsernameOrEmail(String username) {
		Optional<User> user;
		if (username.contains("@")) {
			user = userRepository.findByEmail(username);
		} else {
			user = userRepository.findByUsername(username);
		}
		return user;
	}

	/**
	 * Delete a {@link User} and its dependencies.
	 */
	public void deleteUser(User user) {
		if (user != null) {
			log.info("Delete user: {}", user);
			userRepository.delete(user);
			try {
				pathManagerService.deleteUserStoragePath(user);
			} catch (IOException e) {
				log.error("Coulnd't delete storage directory for user [id=" + user.getId() + ", username=" + user.getUsername() + "]", e);
			}
		}
	}

	/**
	 * Adds a login attempt to a {@link User}. If user reaches max login attempts its account will be locked.
	 */
	public int addLoginAttempt(String username) {
		User user = this.findByUsernameOrEmail(username).orElse(null);

		if (user == null) {
			return -1;
		}

		int attempts = user.getLoginAttempts() + 1;
		int remaining = Math.max(maxLoginAttempts - attempts, 0);
		user.setLoginAttempts(attempts);

		log.debug("User login fail: {}", user);

		if (remaining == 0) {
			user.setStatus(UserStatusType.LOCKED);
			log.info("User locked, reached max login attempts: {}", user);
		}

		user = userRepository.save(user);
		return remaining;
	}

}
