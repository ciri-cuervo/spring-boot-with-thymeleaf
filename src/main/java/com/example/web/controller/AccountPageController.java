/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.domain.EmailHash;
import com.example.domain.types.EmailHashType;
import com.example.service.AccountService;
import com.example.service.EmailHashService;
import com.example.service.MailSenderService;
import com.example.util.ApplicationUtils;
import com.example.util.SecurityUtils;
import com.example.util.WebConstants;
import com.example.util.WebConstants.NavItems;
import com.example.web.command.ActivateCommandValidator;
import com.example.web.command.EmailCommand;
import com.example.web.command.HashCommand;
import com.example.web.command.HashCommandValidator;
import com.example.web.command.PasswordResetCommandValidator;
import com.example.web.command.PasswordResetConfirmCommand;
import com.example.web.command.PasswordResetConfirmCommandValidator;
import com.example.web.command.SignupCommand;
import com.example.web.command.SignupCommandValidator;
import com.example.web.command.UnlockCommandValidator;
import com.example.web.tags.BreadcrumbTag;
import com.example.web.tags.MetaTag;

import lombok.extern.slf4j.Slf4j;

/**
 * Account page controller.
 * 
 * @author ciri-cuervo
 * 
 */
@Controller
@RequestMapping("/account")
@Slf4j
public class AccountPageController {

	private static final String ACCOUNT_HOME_PAGE_ID = "home";

	@Autowired
	private AccountService accountService;
	@Autowired
	private EmailHashService emailHashService;
	@Autowired
	private MailSenderService mailSenderService;

	/*
	 * Validators
	 */

	@Autowired
	private SignupCommandValidator signupCommandValidator;
	@Autowired
	private ActivateCommandValidator activateCommandValidator;
	@Autowired
	private UnlockCommandValidator unlockCommandValidator;
	@Autowired
	private PasswordResetCommandValidator passwordResetCommandValidator;
	@Autowired
	private PasswordResetConfirmCommandValidator passwordResetConfirmCommandValidator;
	@Autowired
	private HashCommandValidator hashCommandValidator;

	/*
	 * Bind validators
	 */

	@InitBinder("signupCommand")
	private void initSignupBinder(WebDataBinder binder) {
		binder.setValidator(signupCommandValidator);
	}

	@InitBinder("activateCommand")
	private void initActivateBinder(WebDataBinder binder) {
		binder.setValidator(activateCommandValidator);
	}

	@InitBinder("unlockCommand")
	private void initUnlockBinder(WebDataBinder binder) {
		binder.setValidator(unlockCommandValidator);
	}

	@InitBinder("passwordResetCommand")
	private void initPasswordResetBinder(WebDataBinder binder) {
		binder.setValidator(passwordResetCommandValidator);
	}

	@InitBinder("passwordResetConfirmCommand")
	private void initPasswordResetConfirmBinder(WebDataBinder binder) {
		binder.setValidator(passwordResetConfirmCommandValidator);
	}

	@InitBinder("hashCommand")
	private void initHashBinder(WebDataBinder binder) {
		binder.setValidator(hashCommandValidator);
	}

	/*
	 * Model attributes
	 */

	@ModelAttribute("regexUsernamePattern")
	private String getRegexUsernamePattern() {
		return ApplicationUtils.REGEX_USERNAME;
	}

	@ModelAttribute("regexEmailPattern")
	private String getRegexEmailPattern() {
		return ApplicationUtils.REGEX_EMAIL;
	}

	@ModelAttribute("regexPasswordPattern")
	private String getRegexPasswordPattern() {
		return ApplicationUtils.REGEX_PASSWORD;
	}

	@ModelAttribute("regexHashPattern")
	private String getRegexHashPattern() {
		return ApplicationUtils.REGEX_HASH;
	}

	/*
	 * Set up model
	 */

	protected void setUpPageInformation(Model model, String page, String... parentPages) {
		model.addAttribute("title", "account." + page + ".title");
		model.addAttribute("header", "account." + page + ".header");

		List<BreadcrumbTag> breadcrumbs = new ArrayList<>();
		for (String parentPage : parentPages) {
			switch (parentPage) {
				case ACCOUNT_HOME_PAGE_ID:
					breadcrumbs.add(new BreadcrumbTag("account.home.header", SecurityUtils.isAuthenticated() ? "/account" : null));
					break;
				default:
					breadcrumbs.add(new BreadcrumbTag("account." + parentPage + ".header", null));
					break;
			}
		}
		if (parentPages.length > 0) {
			breadcrumbs.add(new BreadcrumbTag("account." + page + ".header"));
		}
		model.addAttribute("breadcrumbs", breadcrumbs);

		List<MetaTag> metatags = new ArrayList<>();
		metatags.add(new MetaTag("robots", "none"));
		model.addAttribute("metatags", metatags);
	}

	protected void setUpActiveNavItem(Model model, String activeItem) {
		model.addAttribute("nav_active_" + activeItem, "active");
	}

	/*
	 * Home
	 */

	@RequestMapping(method = RequestMethod.GET)
	public String homePage(Model model) {
		setUpPageInformation(model, ACCOUNT_HOME_PAGE_ID);
		setUpActiveNavItem(model, NavItems.ACCOUNT);
		return WebConstants.Views.Pages.Account.HOME;
	}

	/*
	 * Login
	 */

	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String loginPage(Model model) {
		if (SecurityUtils.isAuthenticated()) {
			return WebConstants.Redirect.TO_HOME;
		}

		setUpPageInformation(model, "login", ACCOUNT_HOME_PAGE_ID);
		setUpActiveNavItem(model, NavItems.LOGIN);
		return WebConstants.Views.Pages.Account.LOGIN;
	}

	/*
	 * Sign Up
	 */

	@RequestMapping(value = "/signup", method = RequestMethod.GET)
	public String signupForm(Model model) {
		if (SecurityUtils.isAuthenticated()) {
			return WebConstants.Redirect.TO_HOME;
		}

		setUpPageInformation(model, "signup", ACCOUNT_HOME_PAGE_ID);
		setUpActiveNavItem(model, NavItems.SIGNUP);
		model.addAttribute(new SignupCommand());
		return WebConstants.Views.Pages.Account.SIGNUP;
	}

	@RequestMapping(value = "/signup", method = RequestMethod.POST)
	public String signupSubmit(@Valid @ModelAttribute SignupCommand signupCommand, BindingResult bindingResult, Model model) {

		if (bindingResult.hasErrors()) {
			setUpPageInformation(model, "signup", ACCOUNT_HOME_PAGE_ID);
			setUpActiveNavItem(model, NavItems.SIGNUP);
			return WebConstants.Views.Pages.Account.SIGNUP;
		}

		EmailHash emailHash = accountService.createUser(signupCommand);

		try {
			mailSenderService.sendHashMail(emailHash, "account.signup.email.subject", WebConstants.Views.Emails.Account.SIGNUP);
		} catch (MessagingException | MailException e) {
			log.error("Error in /signup", e);
			model.addAttribute("errorMsg", "account.signup.error");
			return WebConstants.Views.Pages.Misc.ERROR_MSG_PAGE;
		}

		model.addAttribute("infoMsg", "account.signup.info");
		return WebConstants.Views.Pages.Misc.INFO_MSG_PAGE;
	}

	@RequestMapping(value = "/signup/confirm", method = RequestMethod.GET)
	public String signupConfirmPage(@Valid HashCommand hashCommand, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) throws ServletException {

		if (bindingResult.hasErrors()) {
			model.addAttribute("errorMsg", "account.signupconfirm.error");
			model.addAttribute("errors", bindingResult.getGlobalErrors());
			return WebConstants.Views.Pages.Misc.ERROR_MSG_PAGE;
		}

		request.logout();
		accountService.activateAccount(hashCommand);

		redirectAttributes.addFlashAttribute("infoMsg", "account.signupconfirm.info");
		return WebConstants.Redirect.TO_LOGIN;
	}

	/*
	 * Activate account
	 */

	@RequestMapping(value = "/activate", method = RequestMethod.GET)
	public String activateForm(Model model) {
		setUpPageInformation(model, "activate", ACCOUNT_HOME_PAGE_ID);
		model.addAttribute("activateCommand", new EmailCommand());
		return WebConstants.Views.Pages.Account.ACTIVATE;
	}

	@RequestMapping(value = "/activate", method = RequestMethod.POST)
	public String activateSubmit(@Valid @ModelAttribute("activateCommand") EmailCommand activateCommand, BindingResult bindingResult,
			Model model) {

		if (!bindingResult.hasErrors()) {
			EmailHash emailHash = emailHashService.createEmailHash(activateCommand.getEmail(), EmailHashType.ACCOUNT_ACT);

			try {
				mailSenderService.sendHashMail(emailHash, "account.signup.email.subject", WebConstants.Views.Emails.Account.SIGNUP);
			} catch (MessagingException | MailException e) {
				log.error("Error in /activate", e);
				model.addAttribute("errorMsg", "account.activate.error");
				return WebConstants.Views.Pages.Misc.ERROR_MSG_PAGE;
			}
		}

		model.addAttribute("infoMsg", "account.activate.info");
		return WebConstants.Views.Pages.Misc.INFO_MSG_PAGE;
	}

	/*
	 * Unlock account
	 */

	@RequestMapping(value = "/unlock", method = RequestMethod.GET)
	public String unlockForm(Model model) {
		setUpPageInformation(model, "unlock", ACCOUNT_HOME_PAGE_ID);
		model.addAttribute("unlockCommand", new EmailCommand());
		return WebConstants.Views.Pages.Account.UNLOCK;
	}

	@RequestMapping(value = "/unlock", method = RequestMethod.POST)
	public String unlockSubmit(@Valid @ModelAttribute("unlockCommand") EmailCommand unlockCommand, BindingResult bindingResult,
			Model model) {

		if (!bindingResult.hasErrors()) {
			EmailHash emailHash = emailHashService.createEmailHash(unlockCommand.getEmail(), EmailHashType.ACCOUNT_UNLOCK);

			try {
				mailSenderService.sendHashMail(emailHash, "account.unlock.email.subject", WebConstants.Views.Emails.Account.UNLOCK);
			} catch (MessagingException | MailException e) {
				log.error("Error in /unlock", e);
				model.addAttribute("errorMsg", "account.unlock.error");
				return WebConstants.Views.Pages.Misc.ERROR_MSG_PAGE;
			}
		}

		model.addAttribute("infoMsg", "account.unlock.info");
		return WebConstants.Views.Pages.Misc.INFO_MSG_PAGE;
	}

	@RequestMapping(value = "/unlock/confirm", method = RequestMethod.GET)
	public String unlockConfirmPage(@Valid HashCommand hashCommand, BindingResult bindingResult, Model model,
			RedirectAttributes redirectAttributes, HttpServletRequest request) throws ServletException {

		if (bindingResult.hasErrors()) {
			model.addAttribute("errorMsg", "account.unlockconfirm.error");
			model.addAttribute("errors", bindingResult.getGlobalErrors());
			return WebConstants.Views.Pages.Misc.ERROR_MSG_PAGE;
		}

		request.logout();
		accountService.unlockAccount(hashCommand);

		redirectAttributes.addFlashAttribute("infoMsg", "account.unlockconfirm.info");
		return WebConstants.Redirect.TO_LOGIN;
	}

	/*
	 * Reset Password
	 */

	@RequestMapping(value = "/password/reset", method = RequestMethod.GET)
	public String passwordResetForm(Model model) {
		setUpPageInformation(model, "passwordreset", ACCOUNT_HOME_PAGE_ID);
		model.addAttribute("passwordResetCommand", new EmailCommand());
		return WebConstants.Views.Pages.Account.PASSWORD_RESET;
	}

	@RequestMapping(value = "/password/reset", method = RequestMethod.POST)
	public String passwordResetSubmit(@Valid @ModelAttribute("passwordResetCommand") EmailCommand passwordResetCommand,
			BindingResult bindingResult, Model model) {

		if (!bindingResult.hasErrors()) {
			EmailHash emailHash = emailHashService.createEmailHash(passwordResetCommand.getEmail(), EmailHashType.PWD_RESTORE);

			try {
				mailSenderService.sendHashMail(
						emailHash, "account.passwordreset.email.subject", WebConstants.Views.Emails.Account.PASSWORD_RESET);
			} catch (MessagingException | MailException e) {
				log.error("Error in /password/reset", e);
				model.addAttribute("errorMsg", "account.passwordreset.error");
				return WebConstants.Views.Pages.Misc.ERROR_MSG_PAGE;
			}
		}

		model.addAttribute("infoMsg", "account.passwordreset.info");
		return WebConstants.Views.Pages.Misc.INFO_MSG_PAGE;
	}

	@RequestMapping(value = "/password/reset/confirm", method = RequestMethod.GET)
	public String passwordResetConfirmForm(@Valid HashCommand hashCommand, BindingResult bindingResult, Model model,
			HttpServletRequest request) throws ServletException {

		if (bindingResult.hasErrors()) {
			model.addAttribute("errorMsg", "account.passwordresetconfirm.error");
			model.addAttribute("errors", bindingResult.getGlobalErrors());
			return WebConstants.Views.Pages.Misc.ERROR_MSG_PAGE;
		}

		if (SecurityUtils.isAuthenticated()) {
			request.logout();
			return WebConstants.REDIRECT + ApplicationUtils.getRequestPathWithQuery(request);
		}

		setUpPageInformation(model, "passwordresetconfirm", ACCOUNT_HOME_PAGE_ID);
		model.addAttribute(new PasswordResetConfirmCommand());
		return WebConstants.Views.Pages.Account.PASSWORD_RESET_CONFIRM;
	}

	@RequestMapping(value = "/password/reset/confirm", method = RequestMethod.POST)
	public String passwordResetConfirmSubmit(@Valid @ModelAttribute PasswordResetConfirmCommand passwordResetConfirmCommand,
			BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes) {

		if (bindingResult.hasFieldErrors()) {
			setUpPageInformation(model, "passwordresetconfirm", ACCOUNT_HOME_PAGE_ID);
			return WebConstants.Views.Pages.Account.PASSWORD_RESET_CONFIRM;
		}

		if (bindingResult.hasErrors()) {
			model.addAttribute("errorMsg", "account.passwordresetconfirm.error");
			model.addAttribute("errors", bindingResult.getGlobalErrors());
			return WebConstants.Views.Pages.Misc.ERROR_MSG_PAGE;
		}

		accountService.changePassword(passwordResetConfirmCommand);

		redirectAttributes.addFlashAttribute("infoMsg", "account.passwordresetconfirm.info");
		return WebConstants.Redirect.TO_LOGIN;
	}

}
