/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.tags;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Breadcrumb tag object representation.
 * 
 * @author ciri-cuervo
 * 
 */
@AllArgsConstructor
@Getter
@Setter
public class BreadcrumbTag {

	private String text;
	private String href;

	public BreadcrumbTag(String text) {
		this.text = text;
	}

}
