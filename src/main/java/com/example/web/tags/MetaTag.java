/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.tags;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Meta tag object representation.
 * 
 * @author ciri-cuervo
 * 
 */
@AllArgsConstructor
@Getter
@Setter
public class MetaTag {

	private String name;
	private String content;

}
