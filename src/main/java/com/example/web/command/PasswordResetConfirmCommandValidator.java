/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.command;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.example.domain.EmailHashRepository;

/**
 * Password Reset Confirm command validator.
 * 
 * @author ciri-cuervo
 * 
 */
@Component
public class PasswordResetConfirmCommandValidator implements Validator {

	@Autowired
	private EmailHashRepository emailHashRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return PasswordResetConfirmCommand.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		PasswordResetConfirmCommand command = (PasswordResetConfirmCommand) target;

		if (!errors.hasFieldErrors("hash")) {
			if (!emailHashRepository.findByHashAndActiveTrue(command.getHash()).isPresent()) {
				errors.reject("NotActive.passwordResetConfirmCommand");
			}
		}

		if (!errors.hasFieldErrors("password") && !errors.hasFieldErrors("repeatPassword")
				&& !Objects.equals(command.getPassword(), command.getRepeatPassword())) {
			errors.rejectValue("repeatPassword", "Pattern");
		}
	}

}
