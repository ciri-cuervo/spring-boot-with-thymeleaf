/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.command;

import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.example.util.ApplicationUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Password Reset command.
 * 
 * @author ciri-cuervo
 * 
 */
@Getter
@Setter
@ToString
public class PasswordResetConfirmCommand {

	@Pattern(regexp = ApplicationUtils.REGEX_HASH)
	private String hash;

	@Pattern(regexp = ApplicationUtils.REGEX_PASSWORD)
	private String password;

	@NotEmpty
	private String repeatPassword;

}
