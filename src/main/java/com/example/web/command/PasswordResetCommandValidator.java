/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.command;

import org.springframework.stereotype.Component;

import com.example.domain.types.UserStatusType;

/**
 * Password Reset email command validator.
 * 
 * @author ciri-cuervo
 * 
 */
@Component
public class PasswordResetCommandValidator extends EmailCommandValidator {

	public PasswordResetCommandValidator() {
		super(UserStatusType.ACTIVE);
	}

}
