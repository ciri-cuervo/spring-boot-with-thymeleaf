/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.command;

import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.example.domain.User;
import com.example.domain.UserRepository;

/**
 * Sign-up command validator.
 * 
 * @author ciri-cuervo
 * 
 */
@Component
public class SignupCommandValidator implements Validator {

	@Autowired
	private UserRepository userRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return SignupCommand.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		SignupCommand command = (SignupCommand) target;

		if (!errors.hasFieldErrors("username")) {
			Optional<User> user = userRepository.findByUsername(command.getUsername());
			user.ifPresent(u -> errors.rejectValue("username", "Exists"));
		}

		if (!errors.hasFieldErrors("email")) {
			Optional<User> user = userRepository.findByEmail(command.getEmail());
			user.ifPresent(u -> errors.rejectValue("email", "Exists"));
		}

		if (!errors.hasFieldErrors("password")) {
			if (!errors.hasFieldErrors("username") && Objects.equals(command.getUsername(), command.getPassword())
					|| !errors.hasFieldErrors("email") && Objects.equals(command.getEmail(), command.getPassword())) {
				errors.rejectValue("password", "Invalid");
			}
		}

		if (!errors.hasFieldErrors("password") && !errors.hasFieldErrors("repeatPassword")
				&& !Objects.equals(command.getPassword(), command.getRepeatPassword())) {
			errors.rejectValue("repeatPassword", "Pattern");
		}
	}

}
