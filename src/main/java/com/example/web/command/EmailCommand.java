/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.command;

import javax.validation.constraints.Pattern;

import com.example.util.ApplicationUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Email command.
 * 
 * @author ciri-cuervo
 * 
 */
@Getter
@Setter
@ToString
public class EmailCommand {

	@Pattern(regexp = ApplicationUtils.REGEX_EMAIL)
	private String email;

}
