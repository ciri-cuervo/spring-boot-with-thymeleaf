/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.command;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.example.domain.EmailHash;
import com.example.domain.EmailHashRepository;
import com.example.domain.UserRepository;

/**
 * Hash command validator.
 * 
 * @author ciri-cuervo
 * 
 */
@Component
public class HashCommandValidator implements Validator {

	@Autowired
	private EmailHashRepository emailHashRepository;
	@Autowired
	private UserRepository userRepository;

	@Override
	public boolean supports(Class<?> clazz) {
		return HashCommand.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		HashCommand command = (HashCommand) target;

		if (!errors.hasFieldErrors("hash")) {
			Optional<EmailHash> emailHash = emailHashRepository.findByHashAndActiveTrue(command.getHash());
			if (!emailHash.isPresent() || !userRepository.findByEmail(emailHash.get().getEmail()).isPresent()) {
				errors.reject("NotActive.hashCommand");
			}
		}
	}

}
