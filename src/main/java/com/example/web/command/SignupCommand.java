/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.web.command;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotEmpty;

import com.example.util.ApplicationUtils;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Sign-up command.
 * 
 * @author ciri-cuervo
 * 
 */
@Getter
@Setter
@ToString
public class SignupCommand {

	@Pattern(regexp = ApplicationUtils.REGEX_USERNAME)
	private String username;

	@Pattern(regexp = ApplicationUtils.REGEX_EMAIL)
	private String email;

	@Pattern(regexp = ApplicationUtils.REGEX_PASSWORD)
	private String password;

	@NotEmpty
	private String repeatPassword;

	@AssertTrue
	private boolean accept;

}
