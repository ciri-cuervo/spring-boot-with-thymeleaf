/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.domain;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.example.domain.types.EmailHashType;

/**
 * {@link EmailHash} repository.
 * 
 * @author ciri-cuervo
 * 
 */
@Repository
@Transactional(readOnly = true)
public interface EmailHashRepository extends JpaRepository<EmailHash, Long>, EmailHashRepositoryCustom {

	Optional<EmailHash> findByHash(String hash);

	Optional<EmailHash> findByHashAndActiveTrue(String hash);

	Optional<EmailHash> findByEmailAndTypeAndActiveTrue(String email, EmailHashType type);

	@Transactional
	@Modifying
	Integer deleteByActiveFalse();

}
