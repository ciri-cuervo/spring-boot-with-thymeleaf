/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.domain;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

/**
 * Superclass to derive entity classes from.
 * 
 * @author ciri-cuervo
 * 
 * @param <K> the primary-key class
 */
@MappedSuperclass
@NoArgsConstructor
@EqualsAndHashCode(of = "id")
@ToString
public abstract class AbstractGeneratedIdEntity<K extends Serializable> {

	@Id
	@GeneratedValue
	@NonNull
	@Getter
	@Setter
	protected K id;

	@Column(nullable = false, updatable = false)
	@Getter
	protected LocalDateTime createdDate;

	@Column(nullable = false)
	@Getter
	protected LocalDateTime lastModifiedDate;

	@PrePersist
	public void onCreate() {
		this.createdDate = LocalDateTime.now();
		this.lastModifiedDate = this.createdDate;
	}

	@PreUpdate
	public void onUpdate() {
		this.lastModifiedDate = LocalDateTime.now();
	}

}
