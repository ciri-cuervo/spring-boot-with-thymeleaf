/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.domain;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.util.Assert;

import com.example.domain.types.AuthorityType;
import com.example.domain.types.UserStatusType;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * {@link User} entity.
 * 
 * @author ciri-cuervo
 * 
 */
@Entity
@NoArgsConstructor
@Getter
@Setter
@ToString(callSuper = true, exclude = "password")
public class User extends AbstractGeneratedIdEntity<Long> implements UserDetails {

	private static final long serialVersionUID = 2601721411763278400L;

	@Column(nullable = false, unique = true)
	private String username;

	@Column(nullable = false)
	private String password;

	@Column(nullable = false, unique = true)
	private String email;

	private String firtname;

	private String lastname;

	private String address;

	private String phone;

	private String info;

	private LocalDate dob;

	@Column(nullable = false)
	private Integer loginAttempts = 0;

	@Column(nullable = false, length = 32)
	@Enumerated(EnumType.STRING)
	private UserStatusType status;

	@Column(nullable = false, length = 32)
	@Enumerated(EnumType.STRING)
	@ElementCollection(fetch = FetchType.EAGER)
	private final Set<AuthorityType> authorities = new HashSet<>(0);

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return !UserStatusType.LOCKED.equals(status);
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return UserStatusType.ACTIVE.equals(status);
	}

	@Override
	public List<? extends GrantedAuthority> getAuthorities() {
		return authorities.stream().map(a -> new SimpleGrantedAuthority(a.toString())).collect(Collectors.toList());
	}

	public boolean addAuthority(AuthorityType authority) {
		Assert.notNull(authority);
		return authorities.add(authority);
	}

	public boolean removeAuthority(AuthorityType authority) {
		return authorities.remove(authority);
	}

	public boolean hasAuthority(AuthorityType authority) {
		return authorities.contains(authority);
	}

}
