/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.domain;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Custom {@link com.example.domain.EmailHash EmailHash} repository implementation.
 * 
 * @author ciri-cuervo
 * 
 */
@Repository
@Transactional
public class EmailHashRepositoryImpl implements EmailHashRepositoryCustom {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public Integer deactivateByEmail(String email) {
		String hsql = "update EmailHash e set e.active = false where e.active = true and e.email = :email";
		Query query = entityManager.createQuery(hsql);
		query.setParameter("email", email);
		return executeUpdate(query);
	}

	@Override
	public Integer deactivateByExpired() {
		String hsql = "update EmailHash e set e.active = false where e.active = true and e.expire <= NOW()";
		Query query = entityManager.createQuery(hsql);
		return executeUpdate(query);
	}

	protected Integer executeUpdate(Query query) {
		entityManager.flush();
		Integer result = query.executeUpdate();
		entityManager.clear();
		return result;
	}

}
