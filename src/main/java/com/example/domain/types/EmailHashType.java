/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.domain.types;

/**
 * {@link com.example.domain.EmailHash EmailHash} type.
 * 
 * @author ciri-cuervo
 * 
 */
public enum EmailHashType {

	/**
	 * Account activation {@link com.example.domain.EmailHash EmailHash}.
	 */
	ACCOUNT_ACT,
	/**
	 * Account unlock {@link com.example.domain.EmailHash EmailHash}.
	 */
	ACCOUNT_UNLOCK,
	/**
	 * Password restore {@link com.example.domain.EmailHash EmailHash}.
	 */
	PWD_RESTORE;

}
