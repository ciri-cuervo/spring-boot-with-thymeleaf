/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.domain.types;

/**
 * {@link com.example.domain.User User} authority type.
 * 
 * @author ciri-cuervo
 * 
 */
public enum AuthorityType {

	ROLE_ADMIN, ROLE_USER;

}
