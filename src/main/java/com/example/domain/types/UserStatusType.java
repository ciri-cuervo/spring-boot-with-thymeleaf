/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.domain.types;

/**
 * {@link com.example.domain.User User} status type.
 * 
 * @author ciri-cuervo
 * 
 */
public enum UserStatusType {

	/**
	 * Active account {@link com.example.domain.User User}.
	 */
	ACTIVE,
	/**
	 * Not activated account {@link com.example.domain.User User}.
	 */
	WAITING_ACTIVATION,
	/**
	 * Locked account {@link com.example.domain.User User}.
	 */
	LOCKED;

}
