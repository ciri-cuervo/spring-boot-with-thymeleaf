/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.domain;

import org.springframework.stereotype.Repository;

/**
 * Custom {@link com.example.domain.EmailHash EmailHash} repository.
 * 
 * @author ciri-cuervo
 * 
 */
@Repository
public interface EmailHashRepositoryCustom {

	Integer deactivateByEmail(String email);

	Integer deactivateByExpired();

}
