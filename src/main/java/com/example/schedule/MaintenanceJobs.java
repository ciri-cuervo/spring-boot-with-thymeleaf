/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.schedule;

import java.time.LocalDateTime;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.example.domain.EmailHashRepository;
import com.example.domain.UserRepository;
import com.example.util.ApplicationUtils;

import lombok.extern.slf4j.Slf4j;

/**
 * Scheduled tasks that perform general maintenance.
 * 
 * @author ciri-cuervo
 * 
 */
@Component
@Profile({"dev", "production"})
@Slf4j
public class MaintenanceJobs {

	private static final long ONE_MINUTE = 60 * 1000;
	private static final long ONE_HOUR = 60 * ONE_MINUTE;
	private static final long ONE_DAY = 24 * ONE_HOUR;

	@Autowired
	private Environment environment;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private EmailHashRepository emailHashRepository;

	private int hashExpireDuration;

	@PostConstruct
	public void postConstruct() {
		hashExpireDuration =
				environment.getProperty("application.hash-expire-duration", Integer.class, ApplicationUtils.DEFAULT_HASH_EXPIRE_HS);
	}

	/**
	 * This job deactivates expired email hashes.
	 */
	@Scheduled(initialDelay = ONE_MINUTE, fixedRate = ONE_HOUR)
	public void deactivateExpiredEmailHash() {
		Integer emailHashDeactivated = emailHashRepository.deactivateByExpired();
		if (emailHashDeactivated > 0) {
			log.info("{} EmailHash were/was deactivated", emailHashDeactivated);
		}
	}

	/**
	 * This job deletes deactivated email hashes.
	 */
	@Scheduled(initialDelay = ONE_HOUR, fixedRate = ONE_DAY)
	public void deleteInactiveEmailHash() {
		Integer emailHashDeleted = emailHashRepository.deleteByActiveFalse();
		if (emailHashDeleted > 0) {
			log.info("{} EmailHash were/was deleted", emailHashDeleted);
		}
	}

	/**
	 * This job deletes users waiting for activation.
	 */
	@Scheduled(initialDelay = ONE_HOUR, fixedRate = ONE_DAY)
	public void deleteWaitingActivationUser() {
		LocalDateTime maxCreationTime = LocalDateTime.now().minusHours(hashExpireDuration);
		Integer userDeleted = userRepository.deleteByWaitingActivation(maxCreationTime);
		if (userDeleted > 0) {
			log.info("{} User were/was deleted", userDeleted);
		}
	}

}
