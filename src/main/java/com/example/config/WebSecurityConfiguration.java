/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import com.example.service.security.FailureHandlerService;
import com.example.service.security.SuccessHandlerService;

/**
 * Spring Security configuration.
 * 
 * @author ciri-cuervo
 * 
 */
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private SuccessHandlerService successHandlerService;
	@Autowired
	private FailureHandlerService failureHandlerService;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests()
				.antMatchers("/", "/home").permitAll()

				.antMatchers(HttpMethod.GET,  "/account/login").permitAll() // redirects to home if authenticated
				.antMatchers(HttpMethod.POST, "/account/login").anonymous()

				.antMatchers(HttpMethod.GET,  "/account/signup").permitAll() // redirects to home if authenticated
				.antMatchers(HttpMethod.POST, "/account/signup").anonymous()
				.antMatchers(HttpMethod.GET,  "/account/signup/confirm").permitAll() // logs out if authenticated

				.antMatchers(HttpMethod.GET,  "/account/activate").anonymous()
				.antMatchers(HttpMethod.POST, "/account/activate").anonymous()

				.antMatchers(HttpMethod.GET,  "/account/unlock").anonymous()
				.antMatchers(HttpMethod.POST, "/account/unlock").anonymous()
				.antMatchers(HttpMethod.GET,  "/account/unlock/confirm").permitAll() // logs out if authenticated

				.antMatchers(HttpMethod.GET,  "/account/password/reset").anonymous()
				.antMatchers(HttpMethod.POST, "/account/password/reset").anonymous()
				.antMatchers(HttpMethod.GET,  "/account/password/reset/confirm").permitAll() // logs out if authenticated
				.antMatchers(HttpMethod.POST, "/account/password/reset/confirm").anonymous()

				.antMatchers("/account/**").authenticated()

				.anyRequest().authenticated()
		.and().formLogin()
				.loginPage("/account/login")
				.successHandler(successHandlerService)
				.failureHandler(failureHandlerService)
		.and().logout()
				.logoutUrl("/account/logout")
				.logoutSuccessUrl("/home")
		.and().rememberMe()
				.useSecureCookie(true);
	}

	@Override
	public void configure(WebSecurity web) throws Exception {
		web.ignoring()
				.antMatchers("/webjars/**", "/webjarslocator/**")
				.antMatchers("/robots.txt", "/sitemap.xml");
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth
			.userDetailsService(userDetailsService)
			.passwordEncoder(new BCryptPasswordEncoder());
	}

}
