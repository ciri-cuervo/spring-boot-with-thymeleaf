/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.config;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.embedded.EmbeddedServletContainerCustomizer;
import org.springframework.boot.web.servlet.ErrorPage;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpStatus;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import com.example.util.RequestCacheClearInterceptor;
import com.example.util.WebConstants.Views;

/**
 * Spring Web MVC configuration.
 * 
 * @author ciri-cuervo
 * 
 */
@Configuration
public class WebConfiguration extends WebMvcConfigurerAdapter {

	@Override
	public void addViewControllers(ViewControllerRegistry registry) {
		registry.addRedirectViewController("/", "/home");

		registry.addViewController("/home").setViewName(Views.Pages.Misc.HOME_PAGE);
		registry.addViewController("/400").setViewName(Views.Pages.Error.E400);
		registry.addViewController("/401").setViewName(Views.Pages.Error.E401);
		registry.addViewController("/403").setViewName(Views.Pages.Error.E403);
		registry.addViewController("/404").setViewName(Views.Pages.Error.E404);
		registry.addViewController("/500").setViewName(Views.Pages.Error.E500);
		registry.addViewController("/502").setViewName(Views.Pages.Error.E502);
		registry.addViewController("/503").setViewName(Views.Pages.Error.E503);
		registry.addViewController("/504").setViewName(Views.Pages.Error.E504);
		registry.addViewController("/maintenance").setViewName(Views.Pages.Error.MAINTENANCE);
	}

	/*
	 * Add interceptors.
	 * LocaleChangeInterceptor: intercepts requests that contain the param 'locale', and changes the application locale.
	 */

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(new LocaleChangeInterceptor());
		registry.addInterceptor(new RequestCacheClearInterceptor()); // clears request cache when user navigates the site
	}

	/*
	 * Define 'localeResolver' bean. Stores the locale and time zone in a cookie.
	 */

	@Bean
	@ConditionalOnProperty(prefix = "spring.mvc", name = "locale")
	public LocaleResolver localeResolver(@Value("${spring.mvc.locale}") Locale locale) {
		CookieLocaleResolver localeResolver = new CookieLocaleResolver();
		localeResolver.setDefaultLocale(locale);
		return localeResolver;
	}

	/*
	 * Define 'containerCustomizer' bean. Adds error pages that will be used when handling exceptions.
	 */

	@Bean
	public EmbeddedServletContainerCustomizer containerCustomizer() {
		return c -> {
			ErrorPage ep400 = new ErrorPage(HttpStatus.BAD_REQUEST, "/400");
			ErrorPage ep401 = new ErrorPage(HttpStatus.UNAUTHORIZED, "/401");
			ErrorPage ep403 = new ErrorPage(HttpStatus.FORBIDDEN, "/403");
			ErrorPage ep404 = new ErrorPage(HttpStatus.NOT_FOUND, "/404");
			ErrorPage ep500 = new ErrorPage(HttpStatus.INTERNAL_SERVER_ERROR, "/500");
			ErrorPage ep502 = new ErrorPage(HttpStatus.BAD_GATEWAY, "/502");
			ErrorPage ep503 = new ErrorPage(HttpStatus.SERVICE_UNAVAILABLE, "/503");
			ErrorPage ep504 = new ErrorPage(HttpStatus.GATEWAY_TIMEOUT, "/504");

			c.addErrorPages(ep400, ep401, ep403, ep404, ep500, ep502, ep503, ep504);
		};
	}

}
