/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.util;

/**
 * Class with constants for controllers and views.
 * 
 * @author ciri-cuervo
 * 
 */
public interface WebConstants {

	String FORWARD = "forward:";
	String REDIRECT = "redirect:";

	/**
	 * Class with forward constants.
	 */
	interface Forward {

	}

	/**
	 * Class with redirect constants.
	 */
	interface Redirect {

		String TO_HOME = REDIRECT + "/home";
		String TO_LOGIN = REDIRECT + "/account/login";
	}

	/**
	 * Class with view name constants.
	 */
	interface Views {

		/**
		 * Pages.
		 */
		interface Pages {

			/**
			 * Miscellaneous pages.
			 */
			interface Misc {

				String HOME_PAGE = "pages/misc/miscHomePage";
				String INFO_MSG_PAGE = "pages/misc/miscInfoMsgPage";
				String ERROR_MSG_PAGE = "pages/misc/miscErrorMsgPage";
				String ANGULAR_HOME_PAGE = "pages/misc/miscAngularHomePage";
			}

			/**
			 * Account pages.
			 */
			interface Account {

				String HOME = "pages/account/accountHomePage";
				String LOGIN = "pages/account/accountLoginPage";
				String SIGNUP = "pages/account/accountSignupPage";
				String ACTIVATE = "pages/account/accountActivatePage";
				String UNLOCK = "pages/account/accountUnlockPage";
				String PASSWORD_RESET = "pages/account/accountPasswordResetPage";
				String PASSWORD_RESET_CONFIRM = "pages/account/accountPasswordResetConfirmPage";
				String PROFILE = "pages/account/accountProfilePage";
				String PROFILE_EDIT = "pages/account/accountProfileEditPage";
				String PROFILE_EMAIL_EDIT = "pages/account/accountProfileEmailEditPage";
			}

			/**
			 * Error pages.
			 */
			interface Error {

				String E400 = "pages/error/error400Page";
				String E401 = "pages/error/error401Page";
				String E403 = "pages/error/error403Page";
				String E404 = "pages/error/error404Page";
				String E500 = "pages/error/error500Page";
				String E502 = "pages/error/error502Page";
				String E503 = "pages/error/error503Page";
				String E504 = "pages/error/error504Page";
				String MAINTENANCE = "pages/error/errorMaintenancePage";
			}
		}

		/**
		 * Emails.
		 */
		interface Emails {

			/**
			 * Account emails.
			 */
			interface Account {

				String SIGNUP = "emails/account/accountSignupEmail";
				String UNLOCK = "emails/account/accountUnlockEmail";
				String PASSWORD_RESET = "emails/account/accountPasswordResetEmail";
			}
		}
	}

	/**
	 * Class with navigation items constants.
	 */
	interface NavItems {

		String LOGIN = "login";
		String SIGNUP = "signup";
		String ACCOUNT = "account";
	}

}
