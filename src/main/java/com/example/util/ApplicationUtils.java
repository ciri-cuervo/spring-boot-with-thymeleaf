/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.util;

import java.util.UUID;

import javax.servlet.http.HttpServletRequest;

import org.springframework.util.StringUtils;

import lombok.experimental.UtilityClass;

/**
 * Helper utility class with constants and static methods for general use.
 * 
 * @author ciri-cuervo
 * 
 */
@UtilityClass
public class ApplicationUtils {

	public static final String REGEX_USERNAME = "[a-z0-9-]{4,32}";
	public static final String REGEX_DOMAIN = "((?!-)[A-Za-z0-9-]{1,63}(?<!-)\\.)+[A-Za-z]{2,6}";
	public static final String REGEX_EMAIL = "(?!.{255})[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + REGEX_DOMAIN;
	public static final String REGEX_PASSWORD = "(?=(.*[A-Za-z]){1,})(?=(.*[0-9]){1,}).{6,32}";
	public static final String REGEX_HASH = "[0-9a-zA-Z]{8,256}";

	public static final int DEFAULT_HASH_LENGTH = 24;
	public static final int DEFAULT_HASH_EXPIRE_HS = 24;
	public static final int DEFAULT_MAX_LOGIN_ATTEMPTS = 12;
	public static final int DEFAULT_LOGIN_ATTEMPTS_THRESHOLD = 3;
	public static final int DEFAULT_HTTP_PORT = 80;
	public static final int DEFAULT_HTTPS_PORT = 443;

	public static String createHash() {
		return UUID.randomUUID().toString().replace("-", "").substring(0, DEFAULT_HASH_LENGTH);
	}

	/**
	 * Returns the base URL of the application: {@code http[s]://domain.com[:8080][/contextpath]}
	 */
	public static String getBaseUrl(HttpServletRequest request) {
		int port = request.getServerPort();
		String serverPort = (port == DEFAULT_HTTP_PORT || port == DEFAULT_HTTPS_PORT) ? "" : ":" + port;
		return String.format("%s://%s%s%s", request.getScheme(), request.getServerName(), serverPort, request.getContextPath());
	}

	/**
	 * Obtain the request URI without the context path and with the query string appended.
	 * 
	 * @param request the current request
	 * @return the path requested with the query string appended
	 */
	public static String getRequestPathWithQuery(HttpServletRequest request) {
		String contextPath = request.getContextPath();
		String requestPath = request.getRequestURI().substring(contextPath.length());
		return appendCurrentQueryParams(requestPath, request);
	}

	/**
	 * Append the query string of the current request to the target URL.
	 * 
	 * @param targetUrl the String to append the properties to
	 * @param request the current request
	 * @return the target URL with the query string appended
	 */
	protected static String appendCurrentQueryParams(String targetUrl, HttpServletRequest request) {
		String query = request.getQueryString();
		if (StringUtils.hasText(query)) {
			return targetUrl + '?' + query;
		} else {
			return targetUrl;
		}
	}

}
