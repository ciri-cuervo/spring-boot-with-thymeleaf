/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.util;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.example.util.WebConstants.Views.Pages.Account;
import com.example.util.WebConstants.Views.Pages.Misc;

/**
 * This {@link org.springframework.web.servlet.HandlerInterceptor HandlerInterceptor} clears the request cache generated by a
 * {@link org.springframework.security.access.AccessDeniedException AccessDeniedException}, after the user declines a login action. Used to
 * avoid the redirection to an old requested secure page when the user logs in.
 * 
 * @author ciri-cuervo
 * 
 */
public class RequestCacheClearInterceptor extends HandlerInterceptorAdapter {

	protected RequestCache requestCache;
	protected Set<String> allowedViewNames;

	/**
	 * {@link RequestCacheClearInterceptor} constructor.
	 */
	public RequestCacheClearInterceptor() {
		this.requestCache = new HttpSessionRequestCache();
		this.allowedViewNames = new HashSet<>();
		this.allowedViewNames.add(Account.LOGIN);
		this.allowedViewNames.add(Account.SIGNUP);
		this.allowedViewNames.add(Account.ACTIVATE);
		this.allowedViewNames.add(Account.UNLOCK);
		this.allowedViewNames.add(Account.PASSWORD_RESET);
		this.allowedViewNames.add(Account.PASSWORD_RESET_CONFIRM);
		this.allowedViewNames.add(Misc.INFO_MSG_PAGE);
		this.allowedViewNames.add(Misc.ERROR_MSG_PAGE);
	}

	@Override
	public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView)
			throws Exception {

		// if it is not an allowed view, remove request from cache
		if (!allowedViewNames.contains(modelAndView.getViewName())) {
			requestCache.removeRequest(request, response);
		}
	}

}
