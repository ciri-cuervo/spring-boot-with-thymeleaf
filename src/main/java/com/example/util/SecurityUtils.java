/**
 * Copyright (C) 2015-2016 ciri-cuervo. All Rights Reserved.
 */
package com.example.util;

import java.util.Objects;
import java.util.Optional;

import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.authentication.RememberMeAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.example.domain.User;

import lombok.experimental.UtilityClass;

/**
 * Helper utility class for security.
 * 
 * @author ciri-cuervo
 * 
 */
@UtilityClass
public class SecurityUtils {

	/**
	 * Returns the authenticated {@link User}.
	 */
	public static Optional<User> getAuthenticatedUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (Objects.nonNull(authentication) && authentication.getPrincipal() instanceof User) {
			return Optional.of((User) authentication.getPrincipal());
		}
		return Optional.empty();
	}

	public static boolean isAnonymous() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication instanceof AnonymousAuthenticationToken;
	}

	public static boolean isRememberMeAuthenticated() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication instanceof RememberMeAuthenticationToken;
	}

	public static boolean isAuthenticated() {
		return !isAnonymous();
	}

	public static boolean isFullyAuthenticated() {
		return !isAnonymous() && !isRememberMeAuthenticated();
	}

	/**
	 * Ensures that the authentication principal is anonymous.
	 * 
	 * @throws AccessDeniedException when the authentication principal is not anonymous
	 */
	public static void assertAnonymous() {
		if (!isAnonymous()) {
			throw new AccessDeniedException("Sorry, you are not allowed to access here.");
		}
	}

	/**
	 * Ensures that the authentication principal is remember-me authenticated.
	 * 
	 * @throws AccessDeniedException when the authentication principal is not remember-me authenticated
	 */
	public static void assertRememberMeAuthenticated() {
		if (!isRememberMeAuthenticated()) {
			throw new AccessDeniedException("Sorry, you are not allowed to access here.");
		}
	}

	/**
	 * Ensures that the authentication principal is authenticated.
	 * 
	 * @throws AccessDeniedException when the authentication principal is not authenticated
	 */
	public static void assertAuthenticated() {
		if (!isAuthenticated()) {
			throw new AccessDeniedException("Sorry, you are not allowed to access here.");
		}
	}

	/**
	 * Ensures that the authentication principal is fully authenticated.
	 * 
	 * @throws AccessDeniedException when the authentication principal is not fully authenticated
	 */
	public static void assertFullyAuthenticated() {
		if (!isFullyAuthenticated()) {
			throw new AccessDeniedException("Sorry, you are not allowed to access here.");
		}
	}

}
