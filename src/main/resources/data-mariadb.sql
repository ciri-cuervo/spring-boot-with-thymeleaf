SET FOREIGN_KEY_CHECKS = 0;
TRUNCATE user;
TRUNCATE user_authorities;
TRUNCATE email_hash;
SET FOREIGN_KEY_CHECKS = 1;

INSERT INTO `user` (`username`, `password`, `email`, `firtname`, `lastname`, `address`, `phone`, `info`, `dob`, `login_attempts`, `status`, `created_date`, `last_modified_date`) VALUES
('admin', '$2a$10$tOGRZMnvN1aUjwHtRnJFA.5Ks8AKo1Q8uvFrDgtaASaElM5C5NFci', 'admin@email.com', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'ACTIVE', NOW(), NOW()),
('user', '$2a$10$tOGRZMnvN1aUjwHtRnJFA.5Ks8AKo1Q8uvFrDgtaASaElM5C5NFci', 'user@email.com', NULL, NULL, NULL, NULL, NULL, NULL, 0, 'ACTIVE', NOW(), NOW());

INSERT INTO `user_authorities` (`user_id`, `authorities`) VALUES
(1, 'ROLE_ADMIN');
INSERT INTO `user_authorities` (`user_id`, `authorities`) VALUES
(2, 'ROLE_USER');
