(function(JS) {

	var bindRepeatPasswordValidation = function() {
		$('form input[name=password]').on('change', function() {
			if (this.checkValidity()) {
				$('form input[name=repeatPassword]').attr('pattern', this.value);
			}
		});
	};

	var bindCleanInputsAfterChange = function() {
		$('form div.form-group.has-error').find('input, select, textarea').on('change input paste', function() {
			$(this).closest('div.form-group.has-error').removeClass('has-error');
		});
	};

	var fixAutofocusOnError = function() {
		$('form div.form-group.has-error:first').find('input, select, textarea').first().focus();
	};

	var fixMozErrorMessage = function() {
		$('form').find('input[title], select[title], textarea[title]').each(function() {
			$(this).attr('x-moz-errormessage', $(this).attr('title'))
		});
	};

	var bindLogoutBtnAction = function() {
		$('#logout a').on('click', function(e) {
			e.preventDefault();
			$('#logout form').submit();
		});
	};

	$(function() {
		bindRepeatPasswordValidation();
		bindCleanInputsAfterChange();
		fixAutofocusOnError();
		fixMozErrorMessage();
		bindLogoutBtnAction();
	});

})(JS);
