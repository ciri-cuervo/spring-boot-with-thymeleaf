# Spring Boot with Thymeleaf #

This is a demo webapp used as portfolio.

### Technologies ###

* Java 8
* Tomcat
* Spring Boot
* Hibernate
* Mockito
* Thymeleaf
* WebJars (jQuery, AngularJS, Bootstrap, Font Awesome)
* Checkstyle
* Project Lombok
* Maven

### How do I get set up? ###

+ Download source code
    * `git clone https://bitbucket.org/ciri-cuervo/spring-boot-with-thymeleaf.git`
+ Summary of set up
    * Import as Maven Project in your favorite IDE (Eclipse - IntelliJ IDEA)
+ Deployment instructions
    * Just run `Application.java` as a Java application :)
    * It will be accessible at: `http://localhost:9090/`